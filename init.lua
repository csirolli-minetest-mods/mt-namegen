-- Global respawn namespace
namegen = {}
namegen.path = minetest.get_modpath( minetest.get_current_modname() )
namegen.S = minetest.get_translator( "namegen" )

-- Load files
dofile( namegen.path .. "/api.lua" )
dofile( namegen.path .. "/commands.lua" )

